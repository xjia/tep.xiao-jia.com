all:
	dev_appserver.py .

deploy:
	appcfg.py --email=stfairy@gmail.com update .

clean:
	rm -f *.pyc
