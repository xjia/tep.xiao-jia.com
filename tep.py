import webapp2
import jinja2
import os

from google.appengine.api import users

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class MainPage(webapp2.RequestHandler):
  def get(self):
    template_values = {
        'user': users.get_current_user(),
    }

    template = jinja_environment.get_template('index.html')
    self.response.out.write(template.render(template_values))

class Dashboard(webapp2.RequestHandler):
  def get(self):
    user = users.get_current_user()
    self.response.write(user.nickname())

app = webapp2.WSGIApplication([('/', MainPage), ('/dashboard', Dashboard)], debug=True)
